package sheridan;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsFahrenheitRound() 
	{
		
		assertTrue("Invalid value of Celsius", Celsius.isFahrenheitRound(32));
		
	}
	
	@Test
	public void testIsFahrenheitRoundBoundaryIn() 
	{
		
		assertTrue("Invalid value of Celsius", Celsius.isFahrenheitRound(6));
		
	}
	
	@Test
	public void testIsFahrenheitRoundBoundaryOut() 
	{
		
		assertTrue("Invalid value of Celsius", Celsius.isFahrenheitRound(7));
		
	}
	
	@Test
	public void testIsFahrenheitRoundException() 
	{
		
		assertTrue("Invalid value of Celsius", Celsius.isFahrenheitRound(-1));
		
	}
	
	

}
